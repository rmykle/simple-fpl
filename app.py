from flask import Flask, render_template, jsonify, abort, request
import requests
from database import init_db, session
from settings import seasons
from read_db import season_league
from save_db import fetch_data, update_payment
from config import league_id
from models import League, Player, PlayerLeague
from auth import requires_auth

app = Flask(__name__)
init_db()


@app.route("/admin", methods=["GET", "POST"])
@requires_auth
def admin():
    entry_post = False
    if request.method == "POST":
        submitted_ids = request.form.getlist("playerid")
        update_payment(submitted_ids)
        entry_post = True

    players = PlayerLeague.query.filter(
        PlayerLeague.league_id == league_id).all()

    return render_template(
        "admin.html", players=players, entry_posted=entry_post)


@app.route("/")
def main():
    return index("season")


@app.route("/<season>")
def index(season):

    if season not in seasons:
        abort(404)
    league_data = season_league(league_id, seasons[season])
    if league_data is None:
        abort(404)

    return render_template(
        "index.html",
        data=league_data,
        seasons=seasons,
        selected_season=seasons[season])


@app.route("/debug/<season>")
def debug(season):

    if season not in seasons:
        return "nope season"

    selected_season = seasons[season]

    league_data = season_league(league_id, selected_season)

    if league_data is None:
        return "no league data in database"

    return jsonify(league_data)


@app.route("/fetch/")
def api():
    fetch_data()
    return "ok"


@app.teardown_appcontext
def shutdown_session(exception=None):
    session.remove()
