from database import Base
from sqlalchemy import Column, Integer, String, Boolean, Table, ForeignKey
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import relationship, backref


class PlayerLeague(Base):
    __tablename__ = 'player_league'
    player_id = Column(Integer, ForeignKey('players.id'), primary_key=True)
    league_id = Column(Integer, ForeignKey('leagues.id'), primary_key=True)
    paid = Column(Boolean, default=False)
    player = relationship("Player", backref=backref("player_league"))
    league = relationship("League", backref=backref("league_player"))

    def __init__(self, player=None, league=None):
        self.player = player
        self.league = league


class Player(Base):
    __tablename__ = "players"
    id = Column(Integer, primary_key=True)
    player_name = Column(String(100), nullable=False)
    entry_name = Column(String(100), nullable=False)

    leagues = association_proxy("player_league", "league")
    rounds = relationship("Round")

    def __repr__(self):
        return '<Player %r>' % (self.player_name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class League(Base):
    __tablename__ = "leagues"
    id = Column(Integer, primary_key=True)
    league_name = Column(String(100), nullable=False)
    sum_per_player = Column(Integer, default=0)
    year = Column(Integer)

    players = association_proxy("league_player", "player")

    def __repr__(self):
        return '<League %r>' % (self.league_name)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Round(Base):
    __tablename__ = "rounds"
    player_id = Column(Integer, ForeignKey("players.id"), primary_key=True)
    round = Column(Integer, primary_key=True)
    chip_used = Column(String(15))
    bank = Column(Integer)
    value = Column(Integer)
    transfers = Column(Integer)
    transfer_costs = Column(Integer)
    overall_rank = Column(Integer)
    points = Column(Integer)
    rank = Column(Integer)
    points_on_bench = Column(Integer)

    picks = relationship("Pick")

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Pick(Base):
    __tablename__ = "picks"
    player_id = Column(Integer, ForeignKey("players.id"), primary_key=True)
    round = Column(Integer, ForeignKey("rounds.round"), primary_key=True)
    element_id = Column(Integer, primary_key=True)
    captain = Column(Boolean, default=False)
    vice_captain = Column(Boolean, default=False)
    position = Column(Integer)
    multiplier = Column(Integer)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
