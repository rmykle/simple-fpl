from os import path
import requests
from database import init_db, session
from models import League, Player, Round, Pick, PlayerLeague
from config import league_id
from sqlalchemy import func

root_url = "https://fantasy.premierleague.com/drf/"
league_root_string = root_url + "leagues-classic-standings/{}?phase=1&le-page=1&ls-page=1"
league_string = league_root_string.format(league_id)


def update_payment(submitted_ids):
    for assoc in PlayerLeague.query.all():
        if str(assoc.player_id) in submitted_ids:
            assoc.paid = True
        else:
            assoc.paid = False
    session.commit()


def fetch_data():
    if not path.exists("players.db"):
        init_db()

    json_data = requests.get(league_string).json()

    league_data = json_data["league"]

    current_league = session.query(League).get(league_id)
    finished_round = fetch_current_finished_round()

    if (current_league is None):
        current_league = League(
            id=league_data["id"], league_name=league_data["name"])
        session.add(current_league)

    existing_players_ids = list(map(lambda x: x.id, Player.query.all()))

    players = json_data["standings"]["results"]

    for player in players:
        if player["entry"] not in existing_players_ids:
            player_entity = Player(
                id=player["entry"],
                player_name=player["player_name"],
                entry_name=player["entry_name"])
            current_league.players.append(player_entity)

    for player in current_league.players:
        player_id = player.id

        init_round = session.query(func.max(
            Round.round)).filter_by(player_id=player_id).scalar()
        if init_round is None:
            init_round = 1
        else:
            init_round += 1

        while init_round <= finished_round:
            round_url = root_url + "entry/{}/event/{}/picks".format(
                player.id, init_round)
            round_data_response = requests.get(round_url)
            try:
                round_data = round_data_response.json()
            except ValueError:
                pass  # player started late, do nothing

            entry_history = round_data["entry_history"]
            player_round = Round(
                round=init_round,
                chip_used=round_data["active_chip"],
                bank=entry_history["bank"],
                value=entry_history["value"],
                transfers=entry_history["event_transfers"],
                transfer_costs=entry_history["event_transfers_cost"],
                overall_rank=entry_history["overall_rank"],
                points=entry_history["points"],
                rank=entry_history["rank"],
                points_on_bench=entry_history["points_on_bench"])

            player.rounds.append(player_round)

            for pick in round_data["picks"]:
                player_round.picks.append(
                    Pick(
                        player_id=player_id,
                        round=init_round,
                        element_id=pick["element"],
                        position=pick["position"],
                        captain=pick["is_captain"],
                        vice_captain=pick["is_vice_captain"],
                        multiplier=pick["multiplier"]))

            init_round += 1

    session.commit()

    return "ok"


def fetch_static():
    return requests.get(root_url + "bootstrap-static").json()


def fetch_current_finished_round():
    data = fetch_static()
    for round in range(len(data["events"])):
        event = data["events"][round]
        if (not event["finished"]):
            return round
