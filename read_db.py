from database import session
from models import League, Round, PlayerLeague
from functools import reduce
from settings import chips
import copy


def season_league(selected_league, season):
    league = session.query(League).filter(League.id == selected_league).one()
    if league is None:
        return None
    league_as_dict = league.as_dict()

    paid_players = PlayerLeague.query.filter(
        PlayerLeague.paid == True,
        PlayerLeague.league_id == selected_league).all()

    league_as_dict["pot"] = len(paid_players * 300)

    league_as_dict["players"] = [
        format_player(assoc.player, season["start"], season["end"])
        for assoc in paid_players
    ]

    return league_as_dict


def format_player(player, start, end):
    player_dict = player.as_dict()
    player_rounds = session.query(Round).filter(Round.player_id == player.id,
                                                Round.round >= start,
                                                Round.round <= end).all()

    used_chips_rounds = list(filter(lambda x: x.chip_used, player_rounds))
    chips_template = copy.deepcopy(chips)
    chips_used = Round.query.filter(Round.player_id == player.id,
                                    Round.chip_used != "")
    for round in chips_used:

        if round.chip_used == "wildcard":
            print(chips_template["wildcard_fall"]["used_round"])
            if round.round < 21:
                chips_template["wildcard_fall"]["used_round"] = round.round
            else:
                chips_template["wildcard_spring"]["used_round"] = round.round
        else:
            chips_template[round.chip_used]["used_round"] = round.round
    player_dict["used_chips"] = chips_template
    print(player.player_name, chips_template, "\n\n\n")

    player_dict['season_points'] = sum(
        round.points - round.transfer_costs for round in player_rounds)

    return player_dict


def format_round(round):
    round_dict = round.as_dict()
    round_dict["picks"] = [pick.as_dict() for pick in round.picks]
    return round_dict
