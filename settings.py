seasons = {
    "season": {
        "name": "Sesong",
        "start": 1,
        "end": 38,
        "winners": [1900, 600]
    },
    "fall": {
        "name": "Høst",
        "start": 1,
        "end": 20,
        "winners": [450]
    },
    "christmas": {
        "name": "Jul",
        "start": 18,
        "end": 21,
        "winners": [200]
    },
    "spring": {
        "name": "Vår",
        "start": 21,
        "end": 38,
        "winners": [450]
    },
}

chips = {
    "wildcard_fall": {
        "short_name": "WC",
        "used_round": None
    },
    "wildcard_spring": {
        "short_name": "WC",
        "used_round": None
    },
    "bboost": {
        "short_name": "BB",
        "used_round": None
    },
    "3xc": {
        "short_name": "3X",
        "used_round": None
    },
    "freehit": {
        "short_name": "FH",
        "used_round": None
    }
}
